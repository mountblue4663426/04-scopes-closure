const cacheFunction = require("../cacheFunction.cjs");

let square = (value) => {
  return value * value;
};

let cachingResults = cacheFunction(square);

let testParams = [1, 2, 2, 3, 3];
for (let testValue of testParams) {
  let result = cachingResults(testValue);
  console.log(result);
}
