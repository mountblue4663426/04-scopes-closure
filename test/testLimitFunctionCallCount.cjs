const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

let nTimes = 2;
let callback = () => {
  console.log("invoked the callback function");
};

let limitingFunction = limitFunctionCallCount(callback, nTimes);

for (let invokingCounter = 0; invokingCounter < 5; invokingCounter++) {
  let result = limitingFunction();
  if (result === null) {
    console.log(result, "limited the callback function call and returned null");
  }
}
