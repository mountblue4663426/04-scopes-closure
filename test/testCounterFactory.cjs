const counterFactory = require("../counterFactory.cjs");

let factoryObject = counterFactory();

let incrementedValue = factoryObject.increment();
console.log(`Incremented value is ${incrementedValue}`);

let decrementedValue = factoryObject.decrement();
console.log(`Decremented value is ${decrementedValue}`);
