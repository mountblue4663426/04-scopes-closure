function cacheFunction(cb) {
  const cache = {};

  function cachingResults(callbackArgument) {
    if (cache[callbackArgument]) {
      console.log("Returned value from cache");
      return cache[callbackArgument];
    } else {
      let callbackResult = cb(callbackArgument);
      cache[callbackArgument] = callbackResult;

      console.log("Returned value from callback function");
      return callbackResult;
    }
  }

  return cachingResults;
}

module.exports = cacheFunction;
