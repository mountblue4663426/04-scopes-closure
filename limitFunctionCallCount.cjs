function limitFunctionCallCount(cb, n) {
  let allowedTimes = n;

  function onlyNInvocation() {
    if (allowedTimes > 0) {
      allowedTimes--;
      cb();
    } else {
      return null;
    }
  }

  return onlyNInvocation;
}

module.exports = limitFunctionCallCount;
