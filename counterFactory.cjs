function counterFactory() {
  return {
    counter: 0,
    increment() {
      return ++this.counter;
    },
    decrement() {
      return --this.counter;
    },
  };
}

module.exports = counterFactory;
